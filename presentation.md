---
title: Low code / No code une révolution ?
subtitle: Présentation du low code et de ses enjeux
author: Thomas Brosset
date: 27 février 2024
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# Low code / No code une révolution ?

- Qu'est ce que le low code / no-code ?
- Que faire avec le low code ?
- Pourquoi choisir le low code ?
- Proposition de découpage du besoin
- les limites
- Tendances et solutions
- Faut-il proposer du low code à nos clients ?
- Conclusion

# Développer sans écrire une ligne de code

- Interfaces "glisser-déposer"
- Composants prédéfinis, déjà responsive
- Intégrations et extensions
- outils collaboratifs en ligne (SaaS généralement proposé)
- Modèles d'applications (très souvent proposés)
- utilisable par des "non-développeurs" et des débutants (mais attention à la complexité/pertinence des modèles de données qui seront développés...)

## No code vs. Low code

- No code : pas de code
- Low code : peu de code

Peut-on vraiment se passer de code ?
Une formule mathématique est-elle du code ?

# Que faire avec le low-code

## Des outils low-code pour à peu près tous les besoins

- Sites web et applications mobiles
- Outils de gestion de projet
- Applications métiers
- Interfaces utilisateur et tableaux de bord interactifs
- Applications de e-commerce et de gestion de stocks
- Analyse de données
- Automatisation de tâches

## Pourquoi rechercher des solutions low code / no code ?

- Faire "mieux" que Word/Excel/Mail/Calendrier
  - plus vite ?
  - plus fiable ?
  - besoin d'automatiser des tâches, d'éviter des doubles saisies
- Emergence d'une structure/schéma de travail
  - "simple"
  - que l'on peut (pense pouvoir) décrire/réutiliser
- Besoins considérés comme "génériques"
  - attentes plutot faibles sur la personalisation du rendu (au départ...)

## Low-code et process métiers

- Pour les process métiers, phase "pré ERP"
  - pas de besoin de "tout" un ERP
  - pas de besoin de "tout" un CRM
  - pas de besoin de "tout" un outil de BI

# Pourquoi choisir le low code ?

Décortiquer l'argument du coût:

- temps de développement
- coût d'hébergement / SLA
- attention au modèle de tarification (nombre d'utilisateurs, nombre de données, fonctionnalités ...)
- coût de maintenance

## Le low code demande-t-il moins de compétences techniques ?

- La modélisation des données peut être complexe
- Une mauvaise modélisation peut être très coûteuse (erreurs, difficultés d'évolution ou de migration)
- Tout n'est pas "glisser-déposer", il n'y a pas "que" des interfaces graphiques

Ne pas négliger l'accompagnement métier et la phase de conception

## Les autres arguments avancés

- rapidité de développement : oui, en contrepartie d'un nombre de fonctionnalité pré-défini
- moderne / innovant : beaucoup d'outils nouveaux avec des interfaces léchées au design moderne (car souvent pas de designer UI pour les "petites équipes")
- agilité: permet d'aller très vite... au début. L'utilisateur peut faire ses propres modifications sur l'outil (toujours au début...)
- moins de shadow IT : pas forcément. Le shadow IT ne se limite pas à utiliser un outil non validé par la DSI. Process parallèle, données non sécurisées/partagées... sont aussi du shadow IT
- homogénéité des applicatifs au sein de l'organisation : oui... si on migre les outils existants

## Une pseudo-révolution ?

- apparait dès l'émergence des interfaces utilisateurs graphiques (IDE)
- Access / Filemaker : le premier drag & drop pour créer des applications
- RAD (Rapid Application Development) / VBA (Visual Basic for Applications) : un langage "simple et accessible à tous"

# Proposition de découpage du besoin

## 3 phases clés dans une application

:::: {.columns}
::: {.column width="30%" .smaller}
**Utilisation de données pré-existantes**

- import de fichier ? API ? Scraping ?
- transformation de ces données ? (ex: nettoyage, normalisation, ...)

:::
::: {.column width="30%" .smaller}
**Modification / création de nouvelles données**

- Quelle interface de saisie de données? 
  - formulaire (web, mobile, ...)
    - calendrier
    - code barre
    - photo/video/audio
    - signature
    - géolocalisation
  - évènement (réception d'un mail)
  - import de fichier

:::
::: {.column width="30%" .smaller}
**Exploitation de ces "nouvelles" données**

- consultation web
- export de fichier
- API (jamstack, Qgis)
- pdf (facture, billet, ...)
- mail / sms
- appel téléphonique

Mise en forme / Mise en Page

- tableaux
- graphiques
- cartes
- QR code
- Images
- styles, couleurs, positionnement
- responsive

:::
::::

## Une complexité exponentielle

1. Collaboration et permissions
    - voir les modifications en temps réel
    - Qui peut voir / modifier / supprimer quelles données ?

2. Règles de gestion
    - Validation des données
    - Calculs
    - Automatisations
    - Notifications

3. Tâches automatisées
    - actions réalisées sans intervention de l'utilisateur à une date donnée ou lors d'un évènement donné (ex: envoi de mail, mise à jour de données, ...)

# Les limites

## Maintenance, tests et déploiements

Les gros points faibles des outils low code:

- Maintenance: versionning très souvent absent, pas de diff
- Tests: pas de tests automatisés, pas de tests unitaires, pas d'environnement de test
- Déploiements: rares environnements de développement/pré-production, pas de rollback
- Gestion des données: Couche supplémentaire entre le modèle de données stocké et le modèle de données métier. Cela peut poser des soucis de migration ou d'archivage des données.

## Documentation

Il est difficile de documenter un outil low code. Les outils low code sont souvent utilisés par des personnes qui ne sont pas développeurs. La documentation est souvent absente ou très sommaire. Il est difficile de maintenir une documentation à la fois sur la modélisation métier et sur l'utilisation de l'outil.

## Un pas vers le lock-in et la baisse de productivité

Déport de la logique métier dans une configuration via glisser déposé lié à un outil et non à un langage

L'outil devient une couche supplémentaire qui demande des compétences spécifiques

Le glisser / déposé est efficace pour des tâches simples, mais devient rapidement complexe pour des tâches plus complexes. La productivité peut baisser rapidement.

# Tendances et mots clés associés

- Workflow
- Automatisation
- API
- Productivité
- Collaboration / Travail collaboratif
- Gestion de tâches
- Gestion de projet
- Gestion de processus
- Gestion de données / Base de données
- Développement d'applications / Développement no code

## Low-code orienté base de données

- [*grist](https://www.getgrist.com/)
- [*nocodb](https://www.nocodb.com/)
- [*datami](https://datami.multi.coop/)
- [*locoKit](https://locokit.io/)
- [*baserow](https://baserow.io/)
- [airtable](https://airtable.com/)
- [spreadsheep](https://www.spreadsheep.io/)

## Hybrides Word/Excel/Mail/Calendrier

- [notion](https://www.notion.so/fr-fr)
- [quip](https://quip.com/)
- [coda](https://coda.io/)

## Low code orienté Gestion de projet "simples"

- [smartsheet](https://fr.smartsheet.com/)
- [clickup](https://clickup.com/)
- [ntask](https://www.ntaskmanager.com/)
- [hive](https://hive.com/)
- [monday](https://monday.com/)
- [asana](https://asana.com/)
- [kintone](https://www.kintone.com/)
- [wrike](https://www.wrike.com/)

## Low code orienté Gestion de projet plus complexes

- [caspio](https://www.caspio.com/)
- [mendix](https://www.mendix.com/)
- [trackvia](https://www.trackvia.com/)
- [jotform](https://www.jotform.com/)

## Low-code orienté automatisation / API

- [zapier](https://zapier.com/)
- [n8n](https://n8n.io/)
- [make](https://www.make.com/)
- [ifttt](https://ifttt.com/)
- [supersheets](https://supersheets.io/)

## Low-code orienté développeurs (usines logiciel ?)

- Applications
  - [bubbles](https://bubble.io/)
  - [adalo](https://www.adalo.com/)
  - [pandasuite](https://www.pandasuite.com/)

- Usines logicielles "low" code
  - [simplicité](https://simplicite.fr/)
  - [google appsheet](https://www.appsheet.com/)
  - [zoho](https://www.zoho.com/creator/)
  - [microsoft powerapps](https://powerapps.microsoft.com/fr-fr/)
  - [oracle apex](https://www.oracle.com/fr/application-development/apex/)
  - [stacker](https://www.stackerhq.com/)
  - [outsystems](https://www.outsystems.com/)
  - [appian](https://www.appian.com/)
  - [kissflow](https://kissflow.com/)
  - [quickbase](https://www.quickbase.com/)
  - [pega](https://www.pega.com/fr/products/platform)

## Low-code en extension d'un CRM/ERP

- [*odoo](https://www.odoo.com/fr_FR/)
- [axelor](https://axelor.com/fr/)
- [salesforce](https://www.salesforce.com/products/platform/low-code/)

## Low-code orienté data
  
- [knime](https://www.knime.com/)
- [rapidminer](https://rapidminer.com/)
- [dataiku](https://www.dataiku.com/)
- [parabola](https://parabola.io/)

## Low-code divers
  
- [nodered](https://nodered.org/)
- [tray](https://tray.io/)
- [pipedream](https://pipedream.com/) (workflow api)

# Faut-il proposer du low code à nos clients ?

- Le low-code n'est pas une fin en soi.

- S'intéresser en premier lieu à l'environnement technique du client, et s'appuiyer sur les outils existants.

- S'agit-il d'un quick win ? l'outil va-t-il être utilisé sur le long terme ? va-t-il devoir évoluer ?

- L'organisation est elle ok pour l'usage d'un SaaS ? Quel budget ? Faut-il héberger la solution ?

## Un outil technique ou une expertise métier ?

La complexité a tendance à évoluer au fur et à mesure que l'on s'approprie un sujet.
Exemple: gestion de trésorerie chez multi

Beaucoup d'outils low-code proposent des modèles.
Certains outils low-code sont très proches d'un métier en particulier (ex: gestion de projet, gestion de stock, ...), et sont plutot des outils "super configurables".

## Quelques éléments pour une grille d'analyse

- Attention au stockage et à l'accès aux données
   - les solutions Open Sources permettent l'accès aux données brutes
   - en contre partie, le modèle de données peut être plus complexe (la migration ne sera pas forcément plus simple)
- Attention au coût
    - les coûts de maintenance des solutions Open Sources sont souvent sous-estimés (hébergement, sauvegarde, mise à jour, SLA ...)
    - le coût de développement d'une solution complète est très souvent sous-estimé
- Dès que les fonctionnalités sortent du cadre "générique", le low code peut devenir un cauchemar
  - limiter aux fonctionnalités "simples"

# Conclusion

- Le low code est un outil, pas une fin en soi. 
 Certains outils low-code sont plus proches du super framework que de l'outil de productivité.

- Attention au périmètre: 
Bien prendre en compte les limites d'évolution des outils low code, et limiter à un usage spécifique

Ne pas sous-estimer l'importance de l'accompagnement métier. Le client achète un gateau, pas une cuisine.
Certains outils low-code apportent une forte valeur ajoutée métier du fait des modèles proposés.

Le low code est intéressant pour un MVP, mais attention au passage à l'échelle. Comme pour n'importe quel prototype, le passage en production est souvent plus complexe que prévu et peut être beaucoup plus coûteux que prévu.

Attention au lock-in dès que l'on développe des règles métiers et que l'on complexifie le modèle: la migration vers un autre outil peut être très complexe, même avec un outil low code open source.
